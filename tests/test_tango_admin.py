import os
import pytest
import shutil
import subprocess
import tango

TANGO_ADMIN_PATH = os.environ.get("TANGO_ADMIN_PATH")

def run_tango_admin(args):
    if TANGO_ADMIN_PATH is None:
        raise RuntimeError("No tango_admin path, did you mean to run me from ctest?")

    args = [TANGO_ADMIN_PATH] + args

    print(f'Running "{" ".join(args)}"')

    result = subprocess.run(args, capture_output=True)

    width, _ = shutil.get_terminal_size(fallback=(80, 24))
    if result.stdout:
        print("{:-^{width}}\n{}{:-^{width}}\n"
                .format("tango_admin stdout", result.stdout.decode(errors="replace"), "", width=width))
    if result.stderr:
        print("{:-^{width}}\n{}{:-^{width}}\n"
                .format("tango_admin stderr", result.stderr.decode(errors="replace"), "", width=width))

    return result

def test_ping_database(filedb_server):
    result = run_tango_admin(["--ping-database"])

    assert result.returncode == 0

def test_ping_device(filedb_server, server_config):
    result = run_tango_admin(["--ping-device", server_config["dbdevice"]])

    assert result.returncode == 0

def test_ping_network():
    result = run_tango_admin(['--ping-network', '1'])

    assert result.returncode == 0

def test_add_server(filedb_server, server_config):
    args = ["--add-server", server_config["server"], server_config["class"], ",".join(server_config["devices"])]

    result = run_tango_admin(args)

    assert result.returncode == 0

    db = tango.Database()
    servers = sorted([str(s) for s in db.get_server_list().value_string])
    assert servers == sorted([server_config["server"], server_config["dbserver"]])
    devices = sorted([str(d) for d in db.get_device_name(server_config["class"], "*").value_string])
    assert devices == sorted(server_config["devices"])

def test_check_server(filedb_server, server_config):
    args = ["--check-server", server_config["server"]]

    result = run_tango_admin(args)

    assert result.returncode == 255

    args = ["--check-server", server_config["dbserver"]]

    result = run_tango_admin(args)

    assert result.returncode == 0

def test_delete_server(filedb_server_with_server_added, server_config):
    args = ["--delete-server", server_config["server"]]

    result = run_tango_admin(args)

    assert result.returncode == 0

    db = tango.Database()
    for name in db.get_server_list().value_string:
        assert str(name) != server_config["server"]


def test_check_device(filedb_server, server_config):
    result = run_tango_admin(['--check-device', server_config["devices"][0]])

    assert result.returncode == 255

    result = run_tango_admin(['--check-device', server_config["dbdevice"]])

    assert result.returncode == 0

def test_check_device_exported(filedb_server_with_server_added, server_config):
    result = run_tango_admin(['--check-device-exported', server_config["devices"][0]])

    assert result.returncode == 1

    result = run_tango_admin(['--check-device-exported', server_config["dbdevice"]])

    assert result.returncode == 0

def test_server_list(filedb_server_with_server_added, server_config):
    result = run_tango_admin(['--server-list'])

    assert result.returncode == 0

    servers = sorted(result.stdout.decode(errors="replace").strip().split(" "))
    expected = sorted([server_config["server"].split("/")[0], server_config["dbserver"].split("/")[0]])
    assert servers == expected

def test_server_instance_list(filedb_server_with_server_added, server_config):
    result = run_tango_admin(['--server-instance-list', server_config["server"].split("/")[0]])

    assert result.returncode == 0

    servers = result.stdout.decode(errors="replace").strip().split(" ")
    assert servers == [server_config["server"].split("/")[1]]

def test_add_property(filedb_server_with_server_added, prop_config):
    result = run_tango_admin(['--add-property', prop_config["device"], prop_config["name"], ",".join(prop_config["values"])])

    assert result.returncode == 0

    db = tango.Database()
    properties = db.get_device_property(prop_config["device"], prop_config["name"])
    values = [str(p) for p in properties[prop_config["name"]]]
    assert values == prop_config["values"]

def test_delete_property(filedb_server_with_prop_added, prop_config):
    result = run_tango_admin(['--delete-property', prop_config["device"], prop_config["name"]])

    assert result.returncode == 0

    db = tango.Database()
    properties = db.get_device_property(prop_config["device"], prop_config["name"])
    values = [str(p) for p in properties[prop_config["name"]]]
    assert values == []

def test_unexport_device(filedb_server, server_config):
    result = run_tango_admin(['--unexport-device', server_config["dbdevice"]])

    assert result.returncode == 0

    db = tango.Database()
    info = db.import_device(server_config["dbdevice"])
    assert not info.exported

def test_tac_enabled(filedb_server):
    result = run_tango_admin(['--tac-enabled'])

    # TODO: The logic here seems backwards
    assert result.returncode == 0

    db = tango.Database()
    db.put_property("CtrlSystem", {"Services": ["AccessControl/tango:not/areal/device"]} )

    result = run_tango_admin(['--tac-enabled'])

    assert result.returncode == 1

