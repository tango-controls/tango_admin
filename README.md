# tango_admin

C++ source code for the tango_admin utility
This utility is a Tango database command line interface. Please keep in mind that not all the Tango database features are made available by this tool. Instead of providing a complete interface to the Tango DB it rather servers as a quick way to perform frequent tasks on the Tango DB that admins of a Tango Controls system might have to do.
This means one can:

- Ping the database device server, i.e. the Tango DB
- Check if a device is defined in the Tango DB
- Check if a device, that is defined in the Tango DB, is exported, i.e. if its Device Server is running
- Add a device server for a device class and its Devices to the Tango DB
- Remove a device server for a device class together with its devices from the Tango DB
- Check if a device server is defined in the Tango DB
- Get the names of defined device server from the Tango DB
- Get the instances of a defined defined device server from the Tango DB
- Create a property in the Tango DB
- Delete a property from the Tango DB
- Check if a device is running, i.e. ping() it.
- Check if the network works, i.e. if the host name can be retrieved
- Mark a device in the Tango DB as not exported. Use with care!

## Keeping Up-to-date

This repository uses git submodules.

- Ensure that you use `--recurse-submodules` when cloning:

`git clone --recurse-submodules ...`

- Ensure that updates to git submodules are pulled:

`git pull --recurse-submodules`

## Building

To build, from this directory:

```
cmake -Bbuild -S.
cmake --build build
```

By default (on Linux), CMake will use pkg-config to find your installation of
tango.  This can be disabled by passing -DTango_USE_PKG_CONFIG=OFF to CMake. If
pkg-config cannot find tango, or pkg-config is disabled, CMake will try to
search for tango and its dependencies on your system.  You can provide hints to
cmake using either the -DCMAKE_PREFIX_PATH variable or the -DTango_ROOT variable
(cmake 3.12 or later).

### Notable CMake Variables

| Name                   | Default  | Description                                                            | Notes                            |
| ---                    | ---      | ---                                                                    | ---                              |
| -DTango_USE_PKG_CONFIG | ON       | Use pkg-config to find Tango                                           |                                  |
| -DTango_FORCE_STATIC   | OFF      | Force tango_admin to link against the static libtango library          | Fails if no static library found |
| -DCMAKE_PREFIX_PATH    | ""       | ;-separated list of prefix paths to search for dependencies in         |                                  |
| -DTango_ROOT           | ""       | Prefix path to find Tango dependency                                   | CMake 3.12 or later              |
| -DZeroMQ_ROOT          | ""       | Prefix path to find ZeroMQ dependency                                  | CMake 3.12 or later              |
| -Dcppzmq_ROOT          | ""       | Prefix path to find cppzmq dependency                                  | CMake 3.12 or later              |
| -DomniORB4_ROOT        | ""       | Prefix path to find omniORB4 dependency                                | CMake 3.12 or later              |
